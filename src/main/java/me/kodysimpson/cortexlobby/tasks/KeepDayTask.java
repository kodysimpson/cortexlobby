package me.kodysimpson.cortexlobby.tasks;

import me.kodysimpson.cortexlobby.CortexLobby;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

public class KeepDayTask extends BukkitRunnable {

    CortexLobby plugin;

    public KeepDayTask(CortexLobby plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        String world = plugin.getConfig().getString("lobby-world");
        if (Bukkit.getServer().getWorld(world) != null){
            Bukkit.getServer().getWorld(world).setTime(0L);
        }else{
            System.out.println("[CortexLobby] The world currentlly set as lobby does not exist. Edit the config.yml");
        }
    }
}
