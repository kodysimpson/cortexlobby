package me.kodysimpson.cortexlobby.listeners;

import me.kodysimpson.cortexlobby.CortexLobby;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {

    CortexLobby plugin;

    public PlayerJoin(CortexLobby plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){
        Player p = e.getPlayer();
        if (plugin.getConfig().getBoolean("motd")){
            for (int i = 0; i < plugin.getConfig().getList("motd-message").size(); i++){
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getList("motd-message").get(i).toString()));
            }
        }
    }

}
