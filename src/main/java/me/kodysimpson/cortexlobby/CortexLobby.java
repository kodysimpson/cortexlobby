package me.kodysimpson.cortexlobby;

import me.kodysimpson.cortexlobby.listeners.GoodWeatherListener;
import me.kodysimpson.cortexlobby.listeners.PlayerJoin;
import me.kodysimpson.cortexlobby.tasks.KeepDayTask;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

public final class CortexLobby extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        System.out.println("CORTEX LOBBY HAS STARTED UP!");

        //load config
        getConfig().options().copyDefaults();
        saveDefaultConfig();

        //Register Listeners
        getServer().getPluginManager().registerEvents(new GoodWeatherListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerJoin(this), this);

        BukkitTask keepDayTask = new KeepDayTask(this).runTaskTimer(this, 0L, 100L);
    }

}
